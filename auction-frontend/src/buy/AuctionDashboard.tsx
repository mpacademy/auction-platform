import React, { useCallback, useEffect, useState } from 'react'
import { useActionData, useLoaderData } from 'react-router-dom'
import { Auction } from './Auction'
import { AuctionTile } from './Tile'
import { Container, Spinner } from 'react-bootstrap'
import InfiniteScroll from 'react-infinite-scroll-component'
import { CreateBidModal } from '../bid/CreateBidModal'
import { Bid } from '../bid/bid'
import ToastMessage from '../util/Toast'
import { socket } from '../socket'
import { isExpired } from '../util/format-helper'
import { useInterval } from '../hooks'

export const LIMIT = 20

export const loader = async () => {
  const userId = localStorage.getItem('user')
  const auctions = await fetch(
    `${process.env.REACT_APP_API_URL}/auctions/buyer/${userId}?page=0&limit=${LIMIT}`,
  ).then((res) => res.json())

  return { auctions }
}

type LoadAuctionData = {
  auctions: Auction[]
}

type CreateBidData = {
  bid: Bid
}

export default function AuctionDashboard() {
  const { auctions } = useLoaderData() as LoadAuctionData
  const createData = useActionData() as CreateBidData
  const [data, setData] = useState<Auction[]>(auctions)
  const [page, setPage] = useState(0)
  const [hasMore, setHasMore] = useState(!!auctions.length)
  const [modalShow, setModalShow] = useState(false)
  const [selected, setSelected] = useState<Auction | null>(null)

  useEffect(() => {
    const onUpdate = (bid: Bid & { auction: string }) => {
      setData((oldList) => {
        const index = oldList.findIndex((item) => item.id === bid.auction)
        const update = { ...oldList[index], startPrice: Number(bid.price) }
        // update price in bid modal
        setSelected((oldSelected) =>
          oldSelected?.id === update.id ? update : oldSelected,
        )
        // update price in list tile
        return index !== -1
          ? [...oldList.slice(0, index), update, ...oldList.slice(index + 1)]
          : oldList
      })
    }

    socket.on('update', onUpdate)
  }, [])

  const removeExpiredData = useCallback(() => {
    data.forEach((auction, index) => {
      if (isExpired(auction)) {
        setData((oldList) => [
          ...oldList.slice(0, index),
          { ...auction, deleted: true },
          ...oldList.slice(index + 1),
        ])
        setModalShow(false)
        setTimeout(() => {
          setData(data.filter((item) => item.id !== auction.id))
          if (data.length <= 1) {
            setHasMore(false)
          }
        }, 1000)
      }
    })
  }, [data])

  useInterval(removeExpiredData, 1000)

  const next = async () => {
    const userId = localStorage.getItem('user')
    const nextPage = page + 1
    setPage(nextPage)
    const nextData: Auction[] = await fetch(
      `${process.env.REACT_APP_API_URL}/auctions/buyer/${userId}?page=${nextPage}&limit=${LIMIT}`,
    ).then((res) => res.json())
    if (!nextData.length) {
      setHasMore(false)
    } else {
      setData([...data, ...nextData])
    }
  }

  return (
    <Container>
      <div className="mt-3 mb-1">
        <strong>{data.length ?? 0}</strong> auctions found:
      </div>
      <div id="scrollableDiv" style={{ height: 600, overflow: 'visible' }}>
        <InfiniteScroll
          dataLength={data.length}
          next={next}
          hasMore={hasMore}
          loader={
            <div className="d-flex justify-content-center">
              <Spinner animation="grow" />
            </div>
          }
          endMessage={
            data.length ? (
              <p style={{ textAlign: 'center' }}>
                <b>You have seen it all</b>
              </p>
            ) : (
              ''
            )
          }
          scrollableTarget="scrollableDiv"
        >
          {data.map((auction) => (
            <AuctionTile
              key={auction.id}
              auction={auction}
              disabled={auction.deleted}
              className="tile my-2 rounded-3"
              onClick={() => {
                setSelected(auction)
                setModalShow(true)
              }}
            />
          ))}
        </InfiniteScroll>
      </div>
      {modalShow && (
        <CreateBidModal
          auction={selected as Auction}
          show={modalShow}
          onHide={() => setModalShow(false)}
        />
      )}
      <div className="position-absolute" style={{ top: '10vh', right: 10 }}>
        <ToastMessage
          show={!!createData}
          message={`You successfully created on ${selected?.title}`}
          bg="success"
        />
      </div>
    </Container>
  )
}
