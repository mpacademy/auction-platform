import React, { useCallback, useState } from 'react'
import { Auction } from './Auction'
import { Col, Container, ListGroupItem, Row } from 'react-bootstrap'
import { useInterval } from '../hooks'
import { computeTimeLeft } from '../util/format-helper'
import './Tile.scss'

export function AuctionTile({
  auction,
  disabled,
  onClick,
  className,
}: {
  auction: Auction
  disabled?: boolean
  onClick?: () => void
  className?: string
}) {
  const expiresIn = useCallback((date: string) => computeTimeLeft(date), [])
  const [liveCount, setLiveCount] = useState<string>(
    expiresIn(auction.terminateAt),
  )
  const updateLiveCount = useCallback(
    () => setLiveCount(expiresIn(auction.terminateAt)),
    [],
  )

  useInterval(updateLiveCount, 1000)

  return (
    <ListGroupItem disabled={disabled} onClick={onClick} className={className}>
      <Container>
        <Row className="align-items-center" style={{ height: '125px' }}>
          <Col sm={6}>
            <h3>{auction.title.replace('"', '')}</h3>
            <p className="truncate item">{auction.description}</p>
          </Col>
          <Col className="d-flex justify-content-end">
            <p className="item">{liveCount}</p>
          </Col>
          <Col className="d-flex justify-content-end">
            <p className="item fw-bold">{auction.startPrice + '€'}</p>
          </Col>
        </Row>
      </Container>
    </ListGroupItem>
  )
}
