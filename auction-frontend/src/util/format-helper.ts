import moment from 'moment/moment'
import { Auction } from '../buy/Auction'

export const computeTimeLeft = (date: string) => {
  const diff = moment(date).diff(moment(), 'seconds')
  const suffix = diff < 0 ? 'ago' : 'left'
  const normalized = Math.abs(diff)

  if (normalized < 60) {
    return normalized + 's ' + suffix
  } else if (normalized < 60 * 60) {
    return Math.round(normalized / 60) + 'm ' + suffix
  } else if (normalized < 60 * 60 * 24) {
    return Math.round(normalized / (60 * 60)) + 'h ' + suffix
  } else if (normalized < 60 * 60 * 24 * 7) {
    return Math.round(normalized / (60 * 60 * 24)) + 'd ' + suffix
  } else if (normalized < 60 * 60 * 24 * 7 * 4) {
    return Math.round(normalized / (60 * 60 * 24 * 7)) + 'w ' + suffix
  } else {
    return Math.round(normalized / (60 * 60 * 24 * 7 * 4)) + 'm ' + suffix
  }
}

export const isExpired = (auction: Auction) =>
  // end <= now + 1s
  // updating deleted state takes approx. 1s
  moment(auction.terminateAt).isSameOrBefore(
    moment().add(1, 'seconds'),
    'second',
  )
