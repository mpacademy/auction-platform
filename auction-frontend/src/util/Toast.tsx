import React, { useEffect, useState } from 'react'
import Toast from 'react-bootstrap/Toast'
import ToastHeader from 'react-bootstrap/ToastHeader'
import ToastBody from 'react-bootstrap/ToastBody'

export default function ToastMessage({
  show,
  message,
  bg,
}: {
  show: boolean
  message: string
  bg: string
}) {
  const [showToast, setShowToast] = useState(show)

  useEffect(() => {
    setShowToast(show)
  }, [show])
  return (
    <Toast
      bg={bg}
      onClose={() => setShowToast(false)}
      show={showToast}
      delay={4000}
      autohide
      onExited={() => setShowToast(false)}
    >
      <ToastHeader>
        <strong className="me-auto">Notification</strong>
      </ToastHeader>
      <ToastBody style={{ color: 'white' }}>{message}</ToastBody>
    </Toast>
  )
}
