import React, { useCallback, useState } from 'react'
import { Col, Container, Form, Row } from 'react-bootstrap'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import { Form as RouterForm } from 'react-router-dom'
import { Auction } from '../buy/Auction'
import { computeTimeLeft } from '../util/format-helper'
import { useAuth, useInterval } from '../hooks'
import { ErrorMessage, Formik } from 'formik'
import * as yup from 'yup'
import 'react-datetime-picker/dist/DateTimePicker.css'
import 'react-calendar/dist/Calendar.css'
import 'react-clock/dist/Clock.css'

export async function action({ request }: { request: Request }) {
  const formData = await request.formData()
  const { price, isMaximum, bidder, auction } = Object.fromEntries(formData)

  const body = JSON.stringify({
    price,
    isMaximum,
    bidder,
    auction,
  })

  const res = await fetch(`${process.env.REACT_APP_API_URL}/bids`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: body,
  })
  const bid = await res.json()
  return { bid }
}

export function CreateBidModal({
  auction,
  show,
  onHide,
}: {
  auction: Auction
  show: boolean
  onHide: () => void
}) {
  const { user } = useAuth()
  const expiresIn = useCallback((date: string) => computeTimeLeft(date), [])
  const [liveCount, setLiveCount] = useState<string>(
    expiresIn(auction.terminateAt),
  )
  const updateLiveCount = useCallback(
    () => setLiveCount(expiresIn(auction.terminateAt)),
    [],
  )

  useInterval(updateLiveCount, 1000)

  const schema = yup.object().shape({
    price: yup
      .number()
      .min(
        auction.startPrice + 1,
        `Price has to be more than ${auction.startPrice}`,
      )
      .required('Price is required'),
    isMaximum: yup.boolean().notRequired(),
  })

  return (
    <Modal
      show={show}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <RouterForm method="post" onSubmit={onHide}>
        <Formik
          initialValues={{ price: auction.startPrice, isMaximum: false }}
          onSubmit={() => {}}
          validationSchema={schema}
          validateOnMount
        >
          {({ getFieldProps, isValid }) => (
            <>
              <Modal.Header className="justify-content-center">
                <Modal.Title id="contained-modal-title-vcenter">
                  Bid on auction
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <h3>{auction.title}</h3>

                <p>{auction.description}</p>
                <Container>
                  <Row className="justify-content-center align-items-center">
                    <Col sm={{ span: 5, offset: 1 }}>
                      <p>Seller: {auction.seller.name}</p>
                      <p>Deadline: {liveCount}</p>
                    </Col>
                    <Col sm={{ span: 5, offset: 1 }}>
                      <p>
                        Price:{' '}
                        <span style={{ fontSize: '2rem', fontWeight: 'bold' }}>
                          {auction.startPrice}
                        </span>
                      </p>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={{ span: 5, offset: 1 }}>
                      <Form.Group
                        as={Row}
                        className="mb-3"
                        controlId="formBasicEmail"
                      >
                        <Form.Label sm={4} column>
                          Your offer
                        </Form.Label>
                        <Col sm={4}>
                          <Form.Control
                            type="text"
                            placeholder="€"
                            required
                            {...getFieldProps('price')}
                          />
                        </Col>
                        <ErrorMessage name="price">
                          {(msg) => <div style={{ color: 'red' }}>{msg}</div>}
                        </ErrorMessage>
                      </Form.Group>
                    </Col>
                    <Col sm={{ span: 5, offset: 1 }}>
                      <Form.Group
                        as={Row}
                        className="mb-3"
                        controlId="formBasicEmail"
                      >
                        <Form.Label sm={7} column>
                          Is a maximum offer
                        </Form.Label>
                        <Col sm={5} className="align-self-center">
                          <Form.Check
                            id="custom-switch"
                            type="switch"
                            {...getFieldProps('isMaximum')}
                          />
                        </Col>
                      </Form.Group>
                    </Col>
                  </Row>
                  <Form.Control name="bidder" defaultValue={user?.id} hidden />
                  <Form.Control
                    name="auction"
                    defaultValue={auction.id}
                    hidden
                  />
                </Container>
              </Modal.Body>
              <Modal.Footer className="justify-content-between px-5">
                <Button variant="primary" type="submit" disabled={!isValid}>
                  Bid
                </Button>
                <Button variant="secondary" type="button" onClick={onHide}>
                  Cancel
                </Button>
              </Modal.Footer>
            </>
          )}
        </Formik>
      </RouterForm>
    </Modal>
  )
}
