import { useContext, useEffect, useRef } from 'react'
import { AuthContext } from './auth/AuthProvider'

export const useAuth = () => {
  return useContext(AuthContext)
}

export const useInterval = (callback: () => void, delay: number) => {
  const savedCallback = useRef<() => void>(() => {})

  useEffect(() => {
    savedCallback.current = callback
  }, [callback])

  useEffect(() => {
    const getCurrentCallback = () => {
      savedCallback.current()
    }
    const id = setInterval(getCurrentCallback, delay)
    return () => clearInterval(id)
  }, [])
}
