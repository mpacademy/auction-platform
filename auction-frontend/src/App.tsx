import React, { Suspense, useEffect } from 'react'
import { Await, defer, Outlet, useLoaderData } from 'react-router-dom'
import { AuthProvider } from './auth/AuthProvider'
import { Alert, Spinner } from 'react-bootstrap'
import { User } from './auth/User'
import { useAuth } from './hooks'
import { socket } from './socket'

export const loader = async () => {
  const userId = localStorage.getItem('user')
  if (userId) {
    const fetchUser = fetch(
      `${process.env.REACT_APP_API_URL}/users/${userId}`,
    ).then((res) => res.json())
    return defer({ user: fetchUser })
  }
  return { user: null }
}

type AuthData = {
  user: User
}

export default function App() {
  const { user } = useLoaderData() as AuthData
  const { login } = useAuth()

  useEffect(() => {
    socket.connect()
    return () => {
      socket.close()
    }
  }, [])

  useEffect(() => {
    if (user) {
      login(user)
    }
  }, [user])

  return (
    <Suspense fallback={<Spinner animation="grow" />}>
      <Await
        resolve={user}
        errorElement={
          <Alert variant="danger" dismissible={true}>
            Something went wrong!
          </Alert>
        }
      >
        {(user) => (
          <AuthProvider payload={user}>
            <Outlet />
          </AuthProvider>
        )}
      </Await>
    </Suspense>
  )
}
