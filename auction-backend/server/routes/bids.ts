import { Request, Response, Router } from 'express'
import { DI } from '../app'
import { AuctionEntity } from '../../database/entities'

const router = Router()

export const BidController = router

router.post('/', async (req: Request, res: Response) => {
  try {
    const bid = DI.bidRepository.create(req.body)
    DI.em.persist(bid)

    await DI.em.upsert(AuctionEntity, {
      id: bid.auction.id,
      startPrice: bid.price,
    })

    await DI.em.flush()

    DI.socketServer.emit('update', req.body)

    res.json(bid)
  } catch (e: any) {
    return res.status(400).json({ message: e.message })
  }
})
